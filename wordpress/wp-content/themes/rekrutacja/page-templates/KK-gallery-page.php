<?php
/*
Template Name: kk-gallery
*/
?>
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">
  <!-- <div id="primary" class="content-area"> -->
    <div id="content" class="site-content" role="main">

      <h1 style="text-align: center"><?php echo (types_render_field("tytul", array("output"=>"normal"))); ?></h1>

      <div>
        <a href="<?php echo (types_render_field("zdjecie-galeria", array("output"=>"raw"))); ?>" rel="lightbox-maps_and_blocks"><?php echo (types_render_field("zdjecie-galeria", array("output"=>"html", "size"=>"thumbnail", "align"=>"none", "class"=>"gallery-item ", "style"=>"margin:1em"))); ?></a>
      </div>

    </div><!-- #content -->
  <!-- </div><!-- #primary --> -->
  <?php get_sidebar( 'content' ); ?>
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
