<?php
/*
Template Name: kk-home-page
*/
?>
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">

  <!-- <div id="primary" class="content-area"> -->
    <div id="content" class="site-content" role="main">

      <div>
        <?php echo (types_render_field("opis", array("output"=>"normal"))); ?>
      </div>
      <div>
        <a href="<?php echo (types_render_field("zdjecie", array("output"=>"raw"))); ?>" rel="lightbox"><?php echo (types_render_field("zdjecie", array("output"=>"html", "width"=>"500", "align"=>"center", "alt"=>"blok-z-niebem"))); ?></a>
      </div>


    </div><!-- #content -->
  <!-- </div><!-- #primary --> -->
  <?php get_sidebar( 'content' ); ?>
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
